export let renderDanhSach = (list) => {
  let contentHTML = "";

  list.forEach((monAn) => {
    let contentTr = `
                      <tr >
                         <td>${monAn.id}</td>
                         <td>${monAn.ten}</td>
                         <td>${monAn.loai ? "Mặn" : "Chay"}</td>
                         <td>${monAn.gia.toLocaleString()}</td>
                         <td>${monAn.khuyenMai}</td>
                         <td>0</td>
                         <td>${monAn.tinhTrang ? "Còn" : "Hết"}</td>
                         <td>
                         <button
                         data-toggle="modal" data-target="#exampleModal"
                         onclick="suaMonAn(${
                           monAn.id
                         })" class="btn btn-secondary">Sửa</button>
                         <button onclick="xoaMonAn(${
                           monAn.id
                         })" class="btn btn-danger">Xoá</button>
                         </td>
                      </tr>
                      `;
    contentHTML += contentTr;
  });
  document.getElementById("tbodyFood").innerHTML = contentHTML;
};
// {
//     "ten": "National Creative Coordinator",
//     "loai": false,
//     "gia": 31166,
//     "khuyenMai": 51643,
//     "tinhTrang": false,
//     "hinhAnh": "http://loremflickr.com/640/480/nature",
//     "moTa": "http://loremflickr.com/640/480/food",
//     "id": "1"
// }
export let layThongTinTuForm = () => {
  const foodID = document.getElementById("foodID").value;
  const tenMon = document.getElementById("tenMon").value;
  const loai = document.getElementById("loai").value !== "loai1" ? true : false;
  const giaMon = document.getElementById("giaMon").value;
  const khuyenMai = document.getElementById("khuyenMai").value;
  const tinhTrang =
    document.getElementById("tinhTrang").value !== "0" ? true : false;
  const hinhMon = document.getElementById("hinhMon").value;
  const moTa = document.getElementById("moTa").value;

  return {
    foodID,
    tenMon,
    loai,
    giaMon,
    khuyenMai,
    tinhTrang,
    hinhMon,
    moTa,
  };
};
