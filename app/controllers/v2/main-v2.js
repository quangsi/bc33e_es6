import { MonAnV2 } from "../../models/v2/monAn.model.v2.js";
import { layThongTinTuForm, renderDanhSach } from "./controller.v2.js";

const BASE_URL = "https://62f8b7523eab3503d1da1597.mockapi.io";

let renderTableService = () => {
  axios({
    url: `${BASE_URL}/food`,
    method: "GET",
  })
    .then((res) => {
      renderDanhSach(res.data);
    })
    .catch((err) => {});
};
renderTableService();
document.getElementById("btnThemMon").addEventListener("click", () => {
  let data = layThongTinTuForm();

  let monAn = new MonAnV2(
    data.tenMon,
    data.loai,
    data.giaMon,
    data.khuyenMai,
    data.tinhTrang,
    data.hinhMon,
    data.moTa,
    data.foodID
  );

  axios({
    url: `${BASE_URL}/food`,
    method: "POST",
    data: monAn,
  })
    .then((res) => {
      $("#exampleModal").modal("hide");
      renderTableService();
    })
    .catch((err) => {});
});

function suaMonAn(id) {
  console.log("yes", id);
}
window.suaMonAn = suaMonAn;

function xoaMonAn(id) {
  console.log("yes", id);
}
window.xoaMonAn = xoaMonAn;

/**
 *
 * truthy: {},[]
 *
 *
 * falsy : undefined, null, false, NaN, 0, "".
 *

*/

// let dssv = [];

// if ({}) {
// }

// chạy 1
